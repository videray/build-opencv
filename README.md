
```

plugins {
    id 'maven-publish'
}


configurations {
    debug
    release
}

def someFile = file('somefile.txt')

def txtArtifact = artifacts.add('debug', someFile)

publishing {
    publications {
        maven(MavenPublication) {
            artifact txtArtifact
        }
    }

    repositories {
        mavenLocal()
    }
}


```



```

compileTargets {

    target {
        os = 'android'
        arch = 'arm64-v8a'
        cmakeArgs = ["CMAKE_TOOLCHAIN_FILE=$android_ndk/build/cmake/android.toolchain.cmake",
            'ANDROID_ABI=arm64-v8a', 'ANDROID_NATIVE_API_LEVEL=23']
    }

    target {
        os = 'android'
        arch = 'armeabi-v7a'
        cmakeArgs = ["CMAKE_TOOLCHAIN_FILE=$android_ndk/build/cmake/android.toolchain.cmake",
                'ANDROID_ABI=armeabi-v7a', 'ANDROID_NATIVE_API_LEVEL=23']
    }

    target {
        name = 'linux'
        arch = 'x86-64'
    }
}


```